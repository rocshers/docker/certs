FROM alpine

RUN apk upgrade --update-cache --available && \
    apk add openssl && \
    apk add bash && \
    rm -rf /var/cache/apk/

ENV CERT_KEY_FILENAME=cert.key
ENV CERT_CSR_FILENAME=cert.csr
ENV CERT_CRT_FILENAME=cert.crt
ENV CERT_PEM_FILENAME=cert.pem
ENV CERT_DHPARAM_FILENAME=cert.dhparam.pem

ENV CERT_TARGET_DOMAIN=127.0.0.1

WORKDIR /container

COPY ./src .

ENTRYPOINT [ "./entrypoint.sh" ]