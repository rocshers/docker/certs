#!/bin/sh

SUBJECT="/C=CA/ST=None/L=NB/O=None/CN=\$COMMON_NAME"
NUM_OF_DAYS=999

cat v3.ext | sed s/%%DOMAIN%%/$CERT_TARGET_DOMAIN/g > v3.ext

mkdir -p /dist-container
cd /dist-container

echo "Generate: $CERT_KEY_FILENAME"
openssl genrsa -out $CERT_KEY_FILENAME 2048 2> /dev/null

echo "Generate: $CERT_CSR_FILENAME"
openssl req -new -subj "$SUBJECT" -key $CERT_KEY_FILENAME -out $CERT_CSR_FILENAME 2> /dev/null

echo "Generate: $CERT_CRT_FILENAME"
openssl x509 \
    -req \
    -days $NUM_OF_DAYS \
    -in $CERT_CSR_FILENAME \
    -signkey $CERT_KEY_FILENAME \
    -out $CERT_CRT_FILENAME \
    -extfile /container/v3.ext 2> /dev/null

echo "Generate: $CERT_PEM_FILENAME"
cat $CERT_KEY_FILENAME > $CERT_PEM_FILENAME
cat $CERT_CRT_FILENAME >> $CERT_PEM_FILENAME

echo "Generate: $CERT_DHPARAM_FILENAME"
openssl dhparam -out $CERT_DHPARAM_FILENAME 512 2> /dev/null
