# Self-Signed Certificate Generator

A Docker image for generating self-signed certificates, configurable via environment variables.

## Usage

To use this Docker image, mount the `/dist-container/` directory to your host system. The following environment variables can be used to customize the certificate generation:

### Environment Variables

- `CERT_KEY_FILENAME` - The name of the `key` file in the /dist-container directory
  - Default: `cert.key`

- `CERT_CSR_FILENAME` - The name of the `csr` file in the /dist-container directory
  - Default: `cert.csr`

- `CERT_CRT_FILENAME` - The name of the `crt` file in the /dist-container directory
  - Default: `cert.crt`

- `CERT_PEM_FILENAME` - The name of the `pem` file in the /dist-container directory
  - Default: `cert.pem`

- `CERT_DHPARAM_FILENAME` - The name of the `dhparam` file in the /dist-container directory
  - Default: `cert.dhparam.pem`

- `CERT_TARGET_DOMAIN` - The domain that will be specified in the certificate
  - Default: `localhost`
